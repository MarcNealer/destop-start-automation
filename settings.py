from dotenv import load_dotenv, find_dotenv
import os

load_dotenv((find_dotenv(filename=".edbenv")))

EDB_START = os.getenv("EDB_START")
EDB_SCRIPT_NAME = os.getenv("EDB_SCRIPT_NAME")
EDB_BASE = os.getenv('EDB_BASE_PATH')
EDB_VENV = os.getenv('EDB_VENV')
