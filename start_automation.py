import psutil
from settings import *

def start_automation():
    found = False
    for proc in psutil.process_iter():
        if 'python' in proc.name():
            details = psutil.Process(pid=proc.pid).cmdline()
            if len(details) > 1:
                if details[0] =='python':
                    if EDB_SCRIPT_NAME in details[1]:
                        found = True
    if not found:
        print(EDB_VENV)
        print(EDB_START)
        cmd = "{} {}".format(EDB_VENV, EDB_START)

        os.system(cmd)


if __name__ == '__main__':
    start_automation()