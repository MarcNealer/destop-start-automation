import psutil
from settings import *

def restart_automation():
    for proc in psutil.process_iter():
        if 'python' in proc.name():
            details = psutil.Process(pid=proc.pid).cmdline()
            if len(details) > 1:
                if details[0] == 'python':
                    if EDB_SCRIPT_NAME in details[1]:
                        proc.kill()
    counter = 5
    while counter <= 5:
        counter +=1
        found = False
        for proc in psutil.process_iter():
            if 'python' in proc.name():
                details = psutil.Process(pid=proc.pid).cmdline()
                if len(details) > 1:
                    if details[0] == 'python':
                        if EDB_SCRIPT_NAME in details[1]:
                            found = True
        if not found:
            os.system(EDB_START)
            break
