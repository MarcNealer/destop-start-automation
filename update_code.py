from git import Repo
from settings import *
import psutil
import time

def update_check():
    rep = Repo(EDB_BASE)
    commit1 = rep.commit()
    rep.remotes[0].pull()
    commit2 = rep.commit()
    if commit1 != commit2:
        for proc in psutil.process_iter():
            if 'python' in proc.name():
                details = psutil.Process(pid=proc.pid).cmdline()
                if len(details) > 1:
                    if details[0] == 'python':
                        if EDB_SCRIPT_NAME in details[1]:
                            proc.kill()
        time.sleep(5)
        counter = 0
        while counter <= 5:
            counter += 1
            time.sleep(counter*5)
            found = False
            for proc in psutil.process_iter():
                if 'python' in proc.name():
                    details = psutil.Process(pid=proc.pid).cmdline()
                    if len(details) > 1:
                        if details[0] == 'python':
                            if EDB_SCRIPT_NAME in details[1]:
                                found = True
            if not found:
                os.system("{} && python {}".format(EDB_VENV, EDB_START))
                break

